#!/usr/bin/perl -w

use strict;

use Carp ;

$SIG{__WARN__} = \&confess ;

$ENV{TZ}='Europe/Amsterdam' ;
my $localtime=localtime ;

print <<"EOF" ;
Cache-Control: no-cache
content-type: text/html

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<!-- <meta http-equiv="refresh" content="180"> -->

<style>
th, td {
  padding: 10px;
}
</style>

<link rel=stylesheet href=spottersweg.css>
<link rel="stylesheet" type="text/css" href="chbx.css">

<script type=text/javascript src=colorflights.js></script>
<script type=text/javascript src=loadtable.js></script>

<title>aankomst/vertrektijden Eindhoven Airport</title>
</head>
<body onload="startSequence()">
<span style="font-size:1.5em;">Te zien aan de </span><span style="font-size:2.5em;font-weight:bold;"><a href=https://maps.google.com?q=eindhoven+airport+spottersplek>Spottersweg</a></span>
<span style="font-size:1.5em;">bij Eindhoven Airport</span>
<h4>gegevens afkomstig van
 <a href=http://www.eindhovenairport.nl> http://www.eindhovenairport.nl</a>
</h4>
<br>
<div id=localtime>$localtime</div>
<table border=1 id=element0>
EOF

print << "EOB";
</table>
</body>
</html>
EOB

exit 0;
