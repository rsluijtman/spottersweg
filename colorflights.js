// colorflights.js

'use strict';


function color_flights() {
   
  let colored = 3 ; // row index of colored cell
  let daychange = 0 ; // after mdidnight
  let prevtime; // when time < prevtime -> daychange
  
  let table = document.getElementById('element0');

  let 
      actual,
      dt,
      h,
      hm,
      i,
      j,
      m,
      matches,
      now,
      row,
      rx = /(Vertrokken|Geland) (\d+):(\d+)/,
      state,
      time,
      xh,
      xhm,
      xm
      ;

  now = new Date();
  h   = now.getHours();
  m   = now.getMinutes();
  hm  = m + 60*h ;
  time = 0;

  for ( i = 0 ; i < table.rows.length; i++ ){

    row = table.rows[i];
    // 1: actual time
    prevtime = time ;
    time = row.cells[0].innerHTML ;
    if ( daychange == 0 ) {
      if ( time < prevtime ) {
        daychange = 1 ;
      }
    }
    actual = row.cells[1].innerHTML ;
    state = 'nothing' ;
    // Vertrokken hh:mm
    if ( matches = rx.exec( actual ) ) {
      xh = matches[2] ;
      xm = matches[3] ;
      xhm = (xm*1) + (60*xh); // make sure its an int, not a string
      if ( (hm-xhm) < 15 ) { //  15 minutes
        state = 'blockedOff' ; // anchor blocks removed from wheels, give this
                               // amount of minutes for taxiing before take off
      } else {
        state = 'done' ;
      }
    } else if ( /Geland/.exec( actual ) ) {
      state = 'done' ;
    } else if ( actual == "Vertraagd" ) {
      state = 'delay' ;
    } else if ( /Geannuleerd|Omgeleid/.exec( actual ) ) {
      state = 'failed' ;

    } else if ( actual == 'Boarden' ) {
      state = 'lt60' ;
    // 'Ga naar gate' soms 'via paspoortcontrole'
    } else if ( /Ga naar gate/.exec( actual ) ) {
      state = 'lt60' ;
    } else if ( actual == 'Laatste oproep' ) {
      state = 'lt30' ;
    } else if ( actual == 'Gate gesloten' ) {
      state = 'lt15' ;

    } else {
      // calculate time to action
      let rx2 = /Ver(wacht|traagd) (\d+):(\d+)/ ;
      matches = rx2.exec( actual );

      if ( matches ) {
        xh = matches[2];
        xm = matches[3] ;
      } else {
      // if ( val == '' ) { // fool editor }
      // or any other case not yet identified, use scheduled time
        xh = time.substr( 0,2 ) ;
        xm = time.substr( 3,2 ) ;
      }
      // xhm = (xm*1) + (60*(xh*1 +daychange*24)); // make sure its an int, not a string
      xhm = xm*1 + 60*xh + daychange*1440 ; // make sure its an int, not a string
      dt = xhm-hm ;
      if ( dt < 0 ) {
        state = 'late' ;
      } else if ( dt < 15 ) {
        state = 'lt15' ;
      } else if ( dt < 30 ) {
        state = 'lt30' ;
      } else if ( dt < 60 ) {
        state = 'lt60' ;
      }
    }
    row.cells[ colored ].setAttribute( "class", state ) ;
  }
}
