#!/usr/bin/perl -w

# flightdata.cgi
# data for loadtable.js

use strict;

use Carp ;

$SIG{__WARN__} = \&confess ;

use JSON ; # ::Parse 'parse_json';
use Data::Dump qw(dump);
use Time::HiRes qw( usleep ) ;

sub show($$);

my $debug=0;

my $url='https://www.eindhovenairport.nl/api/flights?pageSize=100&offset=0&type=Full%20page' ;

my $maxage=180 ; # reload only if older then this in seconds, 3 minutes

mkdir 'cache' unless -d 'cache' ;
my $file='cache/eheh.b64' ;
my $output;
my $cmd2="base64 -d $file" ;

my @filestat = stat $file ;
my $age ;
if ( @filestat ) {
  $age=$filestat[9];
} else {
  $age = 0;
}
print STDERR "age: $age\n" if $debug;
my $now = time;
if ( ( $now - $age ) >= $maxage ) {
  my $cmd="curl -s -o $file $url 2>&1"; 
  unlink $file ;
  $output=qx( $cmd );

  my $i=1000 ;
  while ( 1 ) {
    last if -f $file ;
    exit 0  if --$i < 0 ;
    usleep 10;
  } 
}

#print STDERR "i: $i\n" ;

if ( ! -f $file ) {
  print STDERR "$file not found!\n";
  exit(0);
}

my $json = qx( $cmd2 ) ;
#my $data = decode_json( $json ) ;

#print "data:\n$data\n";

#print "\ndump:\n";
#my $str=dump( $data ) ;
#print $str ."\n";

#show( $data, '' ) ;

#my @flights = @{$a->$data->{flights}} ;

sub show($$) {
  my $x=shift;
  my $indent=shift;
  my $ref = ref $x ;
  if ( $ref eq '' ) {
    #print STDERR $indent . " -> $x\n";
    return;
  }
  my $key ;
  #print STDERR $indent . "$x $ref\n";
  if ( $ref eq 'HASH' ) {
    foreach $key ( keys(%$x) ) {
      print $indent ."key: $key\n";
      show $x->{$key}, $indent.' ' ;
    }
  } elsif ( $ref eq 'ARRAY' ) {
    my $n=scalar( @$x ) ;
    for( my $i=0 ; $i<$n ; $i+=1 ) {
      #print STDERR $indent . "index: $i\n" ;
      my $elm = $x->[$i] ;
      &show( $elm, $indent . ' ' ) ;
    }
  } else {
    #print STDERR $indent . "-> $x $ref\n";
  }
} 


#print STDERR "\n\n-----------------------------------\n\n";

$| = 1;

print <<"EOF" ;
content-type: text/plain

EOF
    ;
print $json ;

exit 0;
