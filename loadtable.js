// loadtable.js

'use strict';

var ajaxRequest;


function get_flight_data(){

  try {
    // Opera 8.0+, Firefox, Safari
    ajaxRequest = new XMLHttpRequest();
  } catch (e) {
    // Internet Explorer Browsers
    try{
      ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try{
        ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e){
        // Something went wrong
        alert("Your browser broke!");
        return false;
      }
    }
  }
  ajaxRequest.onreadystatechange = handle_flight_data ;

  let date=new Date();
  ajaxRequest.open("GET", "flightdata.cgi?time=" + date.getTime(), true);
  // only reason for appending time is to get a unique url, so any
  // cached result will be ignored

  try {
    ajaxRequest.send(null);
  } catch( e ) {
    alert( "send failed: " + e.toString() ) ;
  }

 /*
  var i ;
  var field;
  for ( i=0 ; field=document.getElementById( 'ga_time_'+i ) ; i++ ) {
    field.innerHTML = '<img src=Throbber.gif class=cimg>' ;
  }

 */

}


function format(nr,size) {
  let fnr=nr+"" ;
  let diff=size - fnr.length ;
  if ( diff <= 0 ) return nr ;
  let i;
  for ( i=0 ; i<diff ; i++ ) fnr = "0"+fnr ;
  return fnr ;
}

// var names = [ 'flightName', 'callSign' ];
var stats = [ 'baggage', 'checkIn', 'gate' ] ;


function handle_flight_data() {

  if ( ajaxRequest.readyState != 4 || ajaxRequest.status != 200 )
    return ;

  load_table();
  color_flights();
}


function load_table(){

  // remember the filled times
  const checkboxes = [] ; // array with ids of checked checkboxes
  const times = [] ; // array with {id:time}
  let table = document.getElementById('element0');

  let
      cb,
      cbid,
      fnm,
      i,
      tcells,
      tm,
      tmid,
      tminput,
      trows
      ;

  for ( trows = table.rows , i=0 ; i<trows.length ; i++ ) {
    tcells = trows[i].cells ;
    // !assume flightname never occurs more than once on a page! wrong!
    // if it does time or time and date can be used to construct a unique id
    // flightname
    fnm = tcells[3].innerHTML.replace( /&nbsp;/g, '+' ) ;
    // time
    tm = tcells[0].innerHTML.replace( /:/, '_' ) ;
    cbid = `cb_${fnm}_${tm}` ;
    tmid = `time_${fnm}_${tm}` ;
    cb = document.getElementById( cbid ) ;
    if ( cb.checked ) {
      checkboxes.push( cbid ) ;
    }
    tminput = document.getElementById( tmid ) ;
    if ( tminput.value != '' ) {
      times.push( { tmid: tmid,  tm:tminput.value  } ) ;
    }
  }

  table.innerHTML = '' ;

  let
      a,
      flights,
      finds,
      id,
      jsontxt,
      now,
      row,
      statustext,
      time_checkbox,
      timediv,
      time_input,
      timetxt,
      txt,
      txts,
      w
      ;

  // write time of creating the table
  timediv=document.getElementById('localtime');
  now = new Date();
  timetxt =
    now.getFullYear() + '-' + format( now.getMonth() +1, 2) + '-' +
    format( now.getDate(), 2 ) + ' ' + format( now.getHours(), 2 ) + ':' +
    format( now.getMinutes(), 2 ) + ':' + format( now.getSeconds(), 2 ) ;
  timediv.innerHTML = timetxt ;

  txt=ajaxRequest.responseText ;
  jsontxt = JSON.parse( txt ) ;
  flights = jsontxt.flights ;

  for ( let flight of flights ) {
    row = table.insertRow(-1) ;
    finds = /T(\d\d:\d\d):/.exec( flight['publicTimeLT'] )  ;
    cell( finds[1], row ) ;

    if ( 'actualTimeLT' in flight ) {
      let a = /T(\d\d:\d\d):/.exec( flight['actualTimeLT'] ) ;
      if ( a != null ) {
        w = flight['direction'] ==  'A' ? 'geland' : 'vertrokken';
        txt = w + ' ' + a[1] ;
      }
    } if ( 'displayText' in flight &&
             'nl' in flight['displayText'] ) {
        txt = flight['displayText']['nl'];
    } else if ( 'expectedTimeLT' in flight ) {
      let a = /T(\d\d:\d\d):/.exec( flight['expectedTimeLT'] ) ;
      if ( a != null ) {
        txt ='Verwacht ' + a[1] ;
      }
    } else {
      txt = '' ;
    }
    cell( txt, row ) ;

    cell( flight['direction'], row) ;

    htmlcell( flight['flightName'].replace( / +/g, '&nbsp;' ) ,row ) ;

    tm = finds[1].replace( /:/, '_' ) ;
    // id -> flight['flightName'], s/ /+/g
    id = flight['flightName'].replace( / +/g, '+' ) + '_' + tm ;

/*
    time_input =
      `<input type=text id="time_${id}" name="time_${id}" size=2 value="">` ;
    htmlcell( time_input, row ) ;

    time_checkbox =
     `<label class="container">
      <input type="checkbox" id="cb_${id}"
         onclick="settime( 'time_${id}' ,1, 'cb_${id}')">
      <span class="checkmark"></span></label>` ;

    htmlcell( time_checkbox, row ) ;
*/

    time_input =
     // `<span style="margin=10"><input type=text id="time_${id}" name="time_${id}" size=2 value="" style="height: 20px; "></span>` ;
      `<input type=text id="time_${id}" name="time_${id}" size=2 value="" style="height: 18px; ">` ;


    time_checkbox =
     `<label class="container"> <input type="checkbox" id="cb_${id}" onclick="settime( 'time_${id}' ,1, 'cb_${id}')"> <span class="checkmark"></span></label>` ;

    htmlcell( `<span style="display:inline;">${time_input}</span><span style="display:inline;">${time_checkbox}</span>`, row ) ;

    htmlcell( flight['callSign'].replace( / +/g, '&nbsp;' ) ,row ) ;

    if ( flight['direction'] == 'A' ) {
      w = 'origin' ;
    } else if ( flight['direction'] == 'D' ) {
      w = 'destination' ;
    }

    htmlcell( flight['route'][w]['name_NL'].replace( / +/g,'&nbsp;') , row ) ;
    txt = '';
    txts=['','',''];
    i=0;
    for ( let stat of stats ) {
      if ( stat + 'StatusText' in flight &&
           'nl' in flight[ stat+'StatusText' ] ) {
        statustext = flight[ stat + 'StatusText' ]['nl'] ;
      } else {
        statustext = '' ;
      }
      txts[i++]=statustext ;
    }
    if ( txts[0]!='' ) txts[0] = txts[0] + ' ';
    if ( txts[1]!='' ) txts[1] = ' ' + txts[1] + ' ';
    if ( txts[2]!='' ) txts[2] = ' ' + txts[2];
    txt = txts[0]+'-'+txts[1]+'-'+txts[2] ;
    cell( txt ,row ) ;
  }

  // a time can be set without being checked, but checkbox cannot be
  // checked without time, so loop only over
  times.forEach( showTimes );
  checkboxes.forEach( clickChecked ) ;
}


function showTimes( value,index,array ) {
  let elm = document.getElementById( value.tmid ) ;
  if ( elm != undefined ) {
    elm.value = value.tm ;
  }
}

function clickChecked( value, index, array ) {
  let elm = document.getElementById( value ) ;
  if ( elm != undefined ) {
    elm.checked = true ;
  }
}


function htmlcell( html, row ) {
  let newcell = row.insertCell( -1 );
  newcell.innerHTML = html ;
}


function cell( txt, row ) {
  let newcell = row.insertCell( -1 );
  let textnode = document.createTextNode( txt ) ;
  newcell.appendChild( textnode ) ;
}


function startSequence(){
  get_flight_data();
  setInterval( get_flight_data, 180000 ) ;
}


// id: id of edit actual to write the time
// flag: 0 -> text in innerHTML,
//       1 -> text in value
// cb: checkbox id
function settime(id,flag,cbid) {
  let
      cb,
      elm
      ;

  elm=document.getElementById(id);
  cb=document.getElementById(cbid);
  if ( cb.checked ) {
    let now=new Date() ;
    let tijd=format( now.getHours(),2 ) + ":" + format( now.getMinutes(),2) ;
    if ( flag ) elm.value=tijd;
    else elm.innerHTML = tijd ;
  } else {
    if ( flag ) elm.value = "" ;
    else elm.innerHTML = "" ;
  }
}

