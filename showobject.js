// $Id$

function showobject(obj,title){
  var pars;
  var Pars=new Array() ;
  var i ;
  for ( i in obj ) {
    Pars.push(i) ;
  }
//  Pars.sort() ;

  var type ;
  var first=true;
  for ( i=0 ; i<Pars.length ; i++ ) {
    try {
      type=typeof( obj[Pars[i]] ) ;
    }
    catch ( e ) {
      type=' error reading ' + Pars[i] + ': ' + e ;
    }
    if ( first ) {
      pars='';
      first=false;
    } else {
      pars=pars+', ';
    }
    pars=pars+Pars[i]+': ('+ type + ')' ;
    if ( type == 'string' || type == 'number' || type == 'boolean' ) {
      pars=pars+' '+obj[Pars[i]] ;
    }
  }
  if ( title !== undefined ) {
    pars =  title + "\n" + pars ;
  }
  alert( pars ) ;
}
